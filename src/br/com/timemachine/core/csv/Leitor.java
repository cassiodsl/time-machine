package br.com.timemachine.core.csv;

import java.util.List;

interface Leitor {
	
	public List<String> getLinhas(String info);

}
