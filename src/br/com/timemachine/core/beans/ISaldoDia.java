package br.com.timemachine.core.beans;

public interface ISaldoDia {

	public int getData();

	public void setData(int data);

	public Long getTotalDia();

	public void setTotalDia(Long totalDia);

	public Long getSaldoDia();

	public void setSaldoDia(Long saldoDia);
}
