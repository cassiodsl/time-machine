package br.com.timemachine.core.validator;

public class Time24HoursValidator {

  public static boolean validator(String horaMinuto) throws Exception {

    String[] hms = horaMinuto.split(":");
    int horas = Integer.parseInt(hms[0]);
    int minutos = Integer.parseInt(hms[1]);

    if (horas > 23) {
      throw new Exception("Hora inválida");

    }
    if (minutos > 59) {
      throw new Exception("Minuto inválido");
    }

    return true;

  }

}
