package br.com.timemachine.core.teste;

import br.com.timemachine.core.tempo.CalcTempoUtil;

public class Teste002 {

	public static void main(String[] args) {

		long dif = CalcTempoUtil.getInstance().getDiferecaHoras("12:00", "15:30");
		System.out.println("Dif: " + dif);
		double horasDecimal = CalcTempoUtil.getInstance().getHorasDecimal(dif);
		System.out.println(horasDecimal);
	}

}
